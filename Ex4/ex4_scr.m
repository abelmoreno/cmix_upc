kp = 30;
ki = 6;
wo=2*pi*60; 
k=0.01;
ts = 0.000001;
num=[ki*k*wo 0]; 
den=[1 k*wo wo^2]; 
sysD=c2d(tf(num,den),ts,'tustin'); 
[numd,dend]=tfdata(sysD,'v');


model = sim('ex4_cmix');

subplot(3,2,1);
plot(t,vbg);
hold on;
plot(t,vag);
plot(t,vcg);

subplot(3,2,2);
plot(t,ibg);
hold on;
plot(t,iag);
plot(t,icg);

subplot(3,2,3);
plot(t,vbl);
hold on;
plot(t,val);
plot(t,vcl);

subplot(3,2,4);
plot(t,ibl);
hold on;
plot(t,ial);
plot(t,icl);

subplot(3,2,5);
plot(t,vbi);
hold on;
plot(t,vai);
plot(t,vci);

subplot(3,2,6);
plot(t,ibi);
hold on;
plot(t,iai);
plot(t,ici);


figure;
plot(t,pp1);
hold on;
plot(t,pp2);





